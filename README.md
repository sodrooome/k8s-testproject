## k8s on TestProject Agent

**Requirements**

- Kubernetes cluster
- TestProject Agent
- Existing test suite / test collections in TestProject

**Why using TestProject instead Selenium Grid?**

simple answer, to make it easier when we setup multiple environments, we just need to use a testproject agent where there is already a selenium hub in it so we don't need to create a service and deploy a selenium hub instances alone. In here, we don't setup with selenium grid but will use selenium standalone version to provide easier way when to do parallel testing accross different environment

**Caveats**

since the test collection for the TestProject agent only exists in the cloud, integration testing with the source code is currently not possible